package com.alexander.database.data;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import io.reactivex.Flowable;

@Dao
public interface NodeDao {

    @Insert
    void addNode(Node node);

    @Query("SELECT * FROM node")
    Flowable<List<Node>> getAllNodes();

    @Query("SELECT MAX(id) FROM node")
    long getMaxId();

    @Query("SELECT * FROM node WHERE id = :id")
    Node getNode(long id);
}
