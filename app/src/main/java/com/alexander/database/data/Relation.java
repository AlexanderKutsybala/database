package com.alexander.database.data;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

@Entity
public class Relation {

    @PrimaryKey(autoGenerate = true)
    private long id;
    private long parentId;
    private long childId;

    @Ignore
    public Relation(long parentId, long childId) {
        this.parentId = parentId;
        this.childId = childId;
    }

    public Relation(long id, long parentId, long childId) {
        this.id = id;
        this.parentId = parentId;
        this.childId = childId;
    }

    public long getId() {
        return id;
    }

    public long getParentId() {
        return parentId;
    }

    public long getChildId() {
        return childId;
    }
}
