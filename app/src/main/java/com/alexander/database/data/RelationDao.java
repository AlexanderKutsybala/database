package com.alexander.database.data;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface RelationDao {

    @Insert
    void addRelation(Relation relation);

    @Query("Delete FROM relation WHERE parentId = :parentId AND childId = :childId")
    void deleteRelation(long parentId, long childId);

    @Query("SELECT childId FROM relation WHERE parentId = :parentId")
    List<Long> getChildrenId(long parentId);

    @Query("SELECT parentId FROM relation WHERE childId = :childId")
    List<Long> getParentsId(long childId);
}
