package com.alexander.database.data;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

import java.util.List;

@Entity
public class Node {

    @PrimaryKey(autoGenerate = true)
    private long id;
    private int value;
    @Ignore
    private List<Node> children;

    @Ignore
    public Node(int value) {
        this.value = value;
    }

    public Node(long id, int value) {
        this.id = id;
        this.value = value;
    }

    public void setChildren(List<Node> children) {
        this.children = children;
    }

    public long getId() {
        return id;
    }

    public int getValue() {
        return value;
    }

    public List<Node> getChildren() {
        return children;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Node node = (Node) o;
        return id == node.id;
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }
}