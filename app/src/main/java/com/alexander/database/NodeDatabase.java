package com.alexander.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.alexander.database.data.Node;
import com.alexander.database.data.NodeDao;
import com.alexander.database.data.Relation;
import com.alexander.database.data.RelationDao;

@Database(entities = {Node.class, Relation.class}, version = 1, exportSchema = false)
public abstract class NodeDatabase extends RoomDatabase {

    public abstract NodeDao getNoteDao();

    public abstract RelationDao getRelationDao();
}
