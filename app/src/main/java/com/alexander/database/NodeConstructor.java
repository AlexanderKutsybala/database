package com.alexander.database;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Pair;


import com.alexander.database.data.Node;
import com.alexander.database.data.Relation;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

public class NodeConstructor {

    private static NodeConstructor ourInstance;
    private NodeDatabase database;
    private ArrayList<Node> indexedList = new ArrayList<>();

    private NodeConstructor(Context context) {
        database = Room.databaseBuilder(context, NodeDatabase.class, "nodes.db")
                .addCallback(new RoomDatabase.Callback() {
                    @Override
                    public void onCreate(@NonNull SupportSQLiteDatabase db) {
                        super.onCreate(db);
                    }
                }).build();
    }

    public static NodeConstructor getInstance(Context context) {
        if (ourInstance == null) ourInstance = new NodeConstructor(context);
        return ourInstance;
    }

    public void addNode(int value) {
        Completable.fromAction(() -> database.getNoteDao().addNode(new Node(value)))
                .subscribeOn(Schedulers.io()).subscribe();
    }

    public Node getNode(long id) {
        if (indexedList != null) return indexedList.get((int) id);
        else return null;
    }

    public void removeRelation(long parentId, long childId) {
        indexedList.get((int) parentId).getChildren().remove(indexedList.get((int) childId));
        Completable.fromAction(() -> database.getRelationDao().deleteRelation(parentId, childId))
                .subscribeOn(Schedulers.io()).subscribe();
    }

    public void addRelation(long parentId, long childId) throws CyclicRelationsException {
        Node parent = indexedList.get((int) parentId);
        Node child = indexedList.get((int) childId);
        try {
            parent.getChildren().add(child);
            checkCyclicRelations(child, child);
            saveRelation(parentId, childId);
        } catch (CyclicRelationsException e) {
            parent.getChildren().remove(child);
            throw new CyclicRelationsException();
        }
    }

    private void checkCyclicRelations(Node mainNode, Node currentNode) throws CyclicRelationsException {
        for (Node child : currentNode.getChildren()) {
            if (mainNode.equals(child)) throw new CyclicRelationsException();
            checkCyclicRelations(mainNode, child);
        }
    }

    private void saveRelation(long parentId, long childId) {
        Completable.fromAction(() -> database.getRelationDao().addRelation(new Relation(parentId, childId)))
                .subscribeOn(Schedulers.io()).subscribe();
    }

    public List<Pair<Node, Integer>> getChildren(Node selectedNode) {
        ArrayList<Pair<Node, Integer>> resultList = new ArrayList<>();
        for (Node child : selectedNode.getChildren())
            resultList.add(new Pair<>(child, R.color.green));
        addUnrelatedNodes(resultList, selectedNode);
        return resultList;
    }

    public List<Pair<Node, Integer>> getParents(Node selectedNode) {
        ArrayList<Pair<Node, Integer>> resultList = new ArrayList<>();
        for (Node node : indexedList)
            if (node != null && node.getChildren().contains(selectedNode))
                resultList.add(new Pair<>(node, R.color.green));
        addUnrelatedNodes(resultList, selectedNode);
        return resultList;
    }

    private void addUnrelatedNodes(ArrayList<Pair<Node, Integer>> resultList, Node selectedNode) {
        for (Node node : indexedList)
            if (node != null && !node.getChildren().contains(selectedNode) &&
                    !node.equals(selectedNode) && !selectedNode.getChildren().contains(node))
                resultList.add(new Pair<>(node, android.R.color.white));
    }

    public Observable<List<Pair<Node, Integer>>> getAllNodes() {
        return database.getNoteDao().getAllNodes().toObservable().map(nodes -> {
            for (int i = 0; i < database.getNoteDao().getMaxId() + 1; i++) indexedList.add(null);
            for (Node node : nodes) indexedList.set((int) node.getId(), node);
            ArrayList<Pair<Node, Integer>> resultList = new ArrayList<>();
            for (Node node : nodes) {
                boolean hasChildren = addChildren(node);
                boolean hasParents = database.getRelationDao().getParentsId(node.getId()).size() != 0;
                if (hasChildren && hasParents) resultList.add(new Pair<>(node, R.color.red));
                else if (hasChildren) resultList.add(new Pair<>(node, R.color.yellow));
                else if (hasParents) resultList.add(new Pair<>(node, R.color.blue));
                else resultList.add(new Pair<>(node, android.R.color.white));
            }
            return resultList;
        });
    }

    private boolean addChildren(Node node) {
        ArrayList<Node> children = new ArrayList<>();
        for (Long childId : database.getRelationDao().getChildrenId(node.getId()))
            children.add(getNodeById(childId));
        node.setChildren(children);
        return children.size() != 0;
    }

    private Node getNodeById(long id) {
        Node node = indexedList.get((int) id);
        addChildren(node);
        return node;
    }
}