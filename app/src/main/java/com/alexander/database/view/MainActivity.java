package com.alexander.database.view;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.alexander.database.NodeConstructor;
import com.alexander.database.R;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {

    NodeConstructor nodeConstructor;
    RecyclerView recyclerView;
    Disposable disposable;
    FloatingActionButton fab;
    LinearLayout container;
    Button button;
    EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        nodeConstructor = NodeConstructor.getInstance(getApplicationContext());
        initViews();
        addListeners();
        recyclerView.setAdapter(new NodeAdapter((node, position) -> {
            Intent intent = new Intent(this, NodeActivity.class);
            intent.putExtra(NodeActivity.NODE_ID, node.getId());
            startActivity(intent);
        }));
    }

    private void initViews() {
        fab = findViewById(R.id.fab);
        container = findViewById(R.id.container);
        button = findViewById(R.id.button);
        editText = findViewById(R.id.editText);
        recyclerView = findViewById(R.id.recyclerView);
    }

    private void addListeners() {
        fab.setOnClickListener(v -> {
            if (container.getVisibility() != View.VISIBLE) {
                container.setVisibility(View.VISIBLE);
                editText.requestFocus();
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                if (imm != null) imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
            }
        });
        button.setOnClickListener(v -> {
            String value = editText.getText().toString();
            if (!value.equals("")) nodeConstructor.addNode(Integer.valueOf(value));
            container.setVisibility(View.GONE);
            editText.setText("");
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        disposable = nodeConstructor.getAllNodes()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(nodes -> {
                    boolean isUpdate = nodes.size() > recyclerView.getAdapter().getItemCount() &&
                            recyclerView.getAdapter().getItemCount() != 0;
                    ((NodeAdapter) recyclerView.getAdapter()).setNodes(nodes);
                    if (isUpdate) recyclerView.getAdapter().notifyItemInserted(nodes.size() - 1);
                    else recyclerView.getAdapter().notifyDataSetChanged();
                });
    }

    @Override
    protected void onStop() {
        super.onStop();
        disposable.dispose();
    }
}
