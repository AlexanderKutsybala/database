package com.alexander.database.view;


import android.support.v7.widget.RecyclerView;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.alexander.database.R;
import com.alexander.database.data.Node;

import java.util.ArrayList;
import java.util.List;

public class NodeAdapter extends RecyclerView.Adapter<NodeAdapter.ViewHolder> {

    public interface OnNodeClick {
        void onClick(Node node, int position);
    }

    private OnNodeClick listener;
    private List<Pair<Node,Integer>> nodes = new ArrayList<>();

    public NodeAdapter(OnNodeClick listener) {
        this.listener = listener;
    }

    public void setNodes(List<Pair<Node,Integer>> nodes) {
        this.nodes = nodes;
    }

    public void changeColor(int position, int color) {
        nodes.set(position, new Pair<>(nodes.get(position).first, color));
        notifyItemChanged(position);
    }

    @Override
    public NodeAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new NodeAdapter.ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.node_item, parent, false), listener);
    }

    @Override
    public void onBindViewHolder(NodeAdapter.ViewHolder holder, int position) {
        holder.bind(nodes.get(position).first, nodes.get(position).second, position);
    }

    @Override
    public int getItemCount() {
        return nodes.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private OnNodeClick listener;
        private TextView textView;

        private ViewHolder(View view, OnNodeClick listener) {
            super(view);
            this.listener = listener;
            textView = view.findViewById(R.id.text);
        }

        private void bind(Node node, Integer color, int position) {
            textView.setBackgroundColor(textView.getContext().getResources().getColor(color));
            textView.setOnClickListener(v -> listener.onClick(node, position));
            textView.setText(String.format(textView.getContext().getString(R.string.text_node),
                    node.getId(), node.getValue()));
        }
    }
}
