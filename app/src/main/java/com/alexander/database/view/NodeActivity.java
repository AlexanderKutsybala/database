package com.alexander.database.view;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.alexander.database.R;


public class NodeActivity extends AppCompatActivity {

    public static final String NODE_ID = "node_id";

    private long nodeId;
    private ViewPager mViewPager;
    private TabLayout tabLayout;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_node);
        nodeId = getIntent().getLongExtra(NODE_ID, 0);
        initViews();
        addListeners();
        setAdapter();
        toolbar.setTitle(String.format(getString(R.string.title_node), nodeId));
    }

    private void initViews() {
        mViewPager = findViewById(R.id.container);
        tabLayout = findViewById(R.id.tabs);
        toolbar = findViewById(R.id.toolbar);
    }

    private void addListeners() {
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));
    }

    private void setAdapter() {
        mViewPager.setAdapter(new FragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                return TabFragment.newInstance(position + 1, nodeId);
            }

            @Override
            public int getCount() {
                return 2;
            }
        });
    }
}
