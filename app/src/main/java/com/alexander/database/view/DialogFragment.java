package com.alexander.database.view;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;

import com.alexander.database.R;


public class DialogFragment extends android.support.v4.app.DialogFragment {

    private static final String NODE_ID = "node_id";
    public static final String IS_RELATION_EXIST = "is_relation_exist";
    public static final String POSITION = "position";

    public static DialogFragment newInstance(long nodeId, boolean isRelationExist, int position) {
        DialogFragment fragment = new DialogFragment();
        Bundle args = new Bundle();
        args.putLong(NODE_ID, nodeId);
        args.putBoolean(IS_RELATION_EXIST, isRelationExist);
        args.putInt(POSITION, position);
        fragment.setArguments(args);
        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        long nodeId = getArguments().getLong(NODE_ID);
        boolean isRelationExist = getArguments().getBoolean(IS_RELATION_EXIST);
        int position = getArguments().getInt(POSITION);
        builder.setMessage(isRelationExist ? getString(R.string.text_remove_relation) :
                getString(R.string.text_add_relation))
                .setPositiveButton(R.string.dialog_yes, (dialog, which) -> {
                    if (isRelationExist) ((TabFragment) getParentFragment()).removeRelation(nodeId, position);
                    else ((TabFragment) getParentFragment()).addRelation(nodeId, position);
                }).setNegativeButton(R.string.dialog_no, (dialog, id) -> {});
        return builder.create();
    }
}