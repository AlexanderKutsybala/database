package com.alexander.database.view;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.alexander.database.CyclicRelationsException;
import com.alexander.database.NodeConstructor;
import com.alexander.database.R;
import com.alexander.database.data.Node;

import java.util.List;

public class TabFragment extends Fragment {

    private static final String TAB_NUMBER = "section_number";
    private static final String DIALOG_FRAGMENT = "dialog_fragment";
    private static final String NODE_ID = "node_id";

    private NodeConstructor nodeConstructor;
    RecyclerView recyclerView;
    private boolean isChildren;
    private Node node;

    public static TabFragment newInstance(int tabNumber, long nodeId) {
        TabFragment fragment = new TabFragment();
        Bundle args = new Bundle();
        args.putInt(TAB_NUMBER, tabNumber);
        args.putLong(NODE_ID, nodeId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_node, container, false);
        nodeConstructor = NodeConstructor.getInstance(getContext());
        isChildren = getArguments().getInt(TAB_NUMBER) == 1;
        node = nodeConstructor.getNode(getArguments().getLong(NODE_ID));
        initRecyclerView(rootView);
        return rootView;
    }

    private void initRecyclerView(View rootView) {
        recyclerView = rootView.findViewById(R.id.recyclerView);
        recyclerView.setAdapter(new NodeAdapter((node, position) -> {
            boolean isRelationExist = false;
            if (isChildren && this.node.getChildren().contains(node)) isRelationExist = true;
            else if (!isChildren && node.getChildren().contains(this.node)) isRelationExist = true;
            DialogFragment.newInstance(node.getId(), isRelationExist, position)
                    .show(getChildFragmentManager(), DIALOG_FRAGMENT);
        }));
        List<Pair<Node, Integer>> nodes = isChildren ?
                nodeConstructor.getChildren(node) : nodeConstructor.getParents(node);
        ((NodeAdapter) recyclerView.getAdapter()).setNodes(nodes);
    }

    public void addRelation(long nodeId, int position) {
        try {
            if (isChildren) nodeConstructor.addRelation(node.getId(), nodeId);
            else nodeConstructor.addRelation(nodeId, node.getId());
            ((NodeAdapter) recyclerView.getAdapter()).changeColor(position, R.color.green);
        } catch (CyclicRelationsException e) {
            Snackbar.make(getView(), getString(R.string.error_cyclic_relations), Snackbar.LENGTH_LONG).show();
        }
    }

    public void removeRelation(long nodeId, int position) {
        if (isChildren) nodeConstructor.removeRelation(node.getId(), nodeId);
        else nodeConstructor.removeRelation(nodeId, node.getId());
        ((NodeAdapter) recyclerView.getAdapter()).changeColor(position, android.R.color.white);
    }
}
